<?php

$lang['required']			= "Das %s Feld wird benötigt.";
$lang['isset']				= "Das %s Feld muss einen Wert enthalten";
$lang['valid_email']		= "Das %s Feld muss eine gültige E-Mail Adresse enthalten.";
$lang['valid_emails']		= "Das %s Feld muss eine gültige E-Mail Adresse enthalten.";
$lang['valid_url']			= "Das %s Feld muss eine gültige URL enthalten.";
$lang['valid_ip']			= "Das %s Feld muss eine gültige IP Adresse enthalten.";
$lang['min_length']			= "Das %s Feld muss mindestens %s Zeichen lang sein enthalten.";
$lang['max_length']			= "Das %s Feld kann nicht länger als %s Zeichen lang sein.";
$lang['exact_length']		= "Das %s Feld muss genau %s Zeichen lang sein.";
$lang['alpha']				= "Das %s Feld darf nur Buchstaben enhalten.";
$lang['alpha_numeric']		= "Das %s Feld darf nur Buchstaben und Ziffern enthalten";
$lang['alpha_dash']			= "Das %s Feld darf nur Buchstaben, Ziffern, Unterstriche und Gedankenstriche enthalten";
$lang['numeric']			= "Das %s Feld darf nur Nummern enthalten";
$lang['is_numeric']			= "Das %s Feld darf nur Ziffern enthalten";
$lang['integer']			= "Das %s Feld muss eine gerade Zahl enthalten.";
$lang['regex_match']		= "Das %s Feld hat nicht das richtige Format.";
$lang['matches']			= "Das %s Feld gleicht nicht dem %s Feld.";
$lang['is_unique'] 			= "Das %s Feld muss einen individuellen Wert enthalten.";
$lang['is_natural']			= "Das %s Feld muss positive Zahlen enthalten.";
$lang['is_natural_no_zero']	= "Das %s Feld muss Zahlen größer 0 enthalten.";
$lang['decimal']			= "Das %s Feld muss eine Dezimalzahl enthalten.";
$lang['less_than']			= "Das %s Feld muss eine Zahl kleiner %s enthalten.";
$lang['greater_than']		= "Das %s Feld muss eine Zahl größer %s enthalten.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */
/* Translation by: Kovah - www.kovah.de */