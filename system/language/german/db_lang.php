<?php

$lang['db_invalid_connection_str'] = 'Die Datenbankeinstellungen konnten anhand der eingegebenen Daten nicht ermittelt werden.';
$lang['db_unable_to_connect'] = 'Die Datenbankverbindung konnte mit den angegebenen Zugangsdaten nicht hergestellt werden.';
$lang['db_unable_to_select'] = 'Die angegebene Datenbank konnte nicht ausgelesen werden: %s';
$lang['db_unable_to_create'] = 'Die angegebene Datenbank konnte nicht ausgelesen werden: %s';
$lang['db_invalid_query'] = 'Die übermittelte Anfrage ist nicht gültig.';
$lang['db_must_set_table'] = 'Es muss eine Tabelle für die Anfrage angegeben werden.';
$lang['db_must_use_set'] = 'Die "set" Methode muss zum Aktualisieren eines Eintrages verwendet werden.';
$lang['db_must_use_index'] = 'Es muss ein passender Index für diese Massenaktualisierung angegeben werden.';
$lang['db_batch_missing_index'] = 'Einer oder mehreren Reihen dieser Massenaktualisierung fehlt der angegebene Index.';
$lang['db_must_use_where'] = 'Aktualisierungen sind nur erlaubt, sofern sie die "where" Klausel enthalten.';
$lang['db_del_must_use_where'] = 'Löschungen sind nur erlaubt, sofern sie die "where" oder "like" Klausel enthalten.';
$lang['db_field_param_missing'] = 'Um Felder abzurufen, muss der Name der Tabelle als Parameter angegeben werden.';
$lang['db_unsupported_function'] = 'Diese Funktion wird von der verwendeten Datenbank nicht unterstützt.';
$lang['db_transaction_failure'] = 'Übermittlungsfehler: Wiederholung wird durchgeführt.';
$lang['db_unable_to_drop'] = 'Die angegebene Datenbank konnte nicht gelöscht werden.';
$lang['db_unsuported_feature'] = 'Dieses Feature wird von der verwendeten Datenbank nicht unterstützt.';
$lang['db_unsuported_compression'] = 'Die angegebene Dateikompression wird nicht vom Server unterstützt.';
$lang['db_filepath_error'] = 'Es konnten keine Daten im angegebenen Pfad geschrieben werden.';
$lang['db_invalid_cache_path'] = 'Der angegebene Pfad für den Cache ist nicht beschreibbar.';
$lang['db_table_name_required'] = 'Für diese Operation wird ein Tabellenname benötigt.';
$lang['db_column_name_required'] = 'Für diese Operation wird ein Spaltenname benötigt.A column name is required for that operation.';
$lang['db_column_definition_required'] = 'Für diese Operation wird eine Spaltendefinition benötigt.';
$lang['db_unable_to_set_charset'] = 'Der Zeichensatz für die Kundenverbindung konnte nicht gesetzt werden: %s';
$lang['db_error_heading'] = 'Ein Datenbankfehler ist aufgetreten';

/* End of file db_lang.php */
/* Location: ./system/language/english/db_lang.php */
/* Translation by: Kovah - www.kovah.de */