<?php

$lang['profiler_database']		= 'DATENBANK';
$lang['profiler_controller_info'] = 'KLASSE/METHODE';
$lang['profiler_benchmarks']	= 'BENCHMARKS';
$lang['profiler_queries']		= 'ANFRAGEN';
$lang['profiler_get_data']		= 'GET DATEN';
$lang['profiler_post_data']		= 'POST DATEN';
$lang['profiler_uri_string']	= 'URI STRING';
$lang['profiler_memory_usage']	= 'SPEICHERVERBRAUCH';
$lang['profiler_config']		= 'EINSTELLUNGSVARIABLEN';
$lang['profiler_session_data']	= 'SITZUNGSDATEN';
$lang['profiler_headers']		= 'HTTP KOPFDATEN';
$lang['profiler_no_db']			= 'Datenbanktreiber wurden nicht geladen';
$lang['profiler_no_queries']	= 'Keine Abfragen wurden getätigt';
$lang['profiler_no_post']		= 'Es existieren keine POST Daten';
$lang['profiler_no_get']		= 'Es existieren keine GET Daten';
$lang['profiler_no_uri']		= 'Es existieren keine URI Daten';
$lang['profiler_no_memory']		= 'Speicherverbrauch nicht verfügbar';
$lang['profiler_no_profiles']	= 'Keine Profildaten - Alle Profilanalysen deaktiviert';
$lang['profiler_section_hide']	= 'Verstecken';
$lang['profiler_section_show']	= 'Anzeigen';

/* End of file profiler_lang.php */
/* Location: ./system/language/english/profiler_lang.php */
/* Translation by: Kovah - www.kovah.de */