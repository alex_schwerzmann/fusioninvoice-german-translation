<?php

$lang['imglib_source_image_required'] = "Es muss ein Quellbild in den Einstellungen angegeben werden.";
$lang['imglib_gd_required'] = "Die GD Bildbibliothek wird für diese Funktion benötigt.";
$lang['imglib_gd_required_for_props'] = "Der Server muss die GD Bildbibliothek unterstützen, um die Bildinformationen lesen zu können.";
$lang['imglib_unsupported_imagecreate'] = "Der Server muss die GD Funktion unterstützen, um diesen Bildtyp verarbeiten zu können.";
$lang['imglib_gif_not_supported'] = "GIF Bilder werden durch Lizenzbestimmungen nicht unterstützt. Bitte JPG oder PNG nutzen.";
$lang['imglib_jpg_not_supported'] = "JPG Bilder werden nicht unterstützt.";
$lang['imglib_png_not_supported'] = "PNG Bilder werden nicht unterstützt.";
$lang['imglib_jpg_or_png_required'] = "Das in den Einstellungen gewählte Protokoll zum Ändern der Bildgröße unterstützt nur JPG und PNG Bilder.";
$lang['imglib_copy_error'] = "Es gab einen Fehler beim Ersetzen der Datei. Bitte prüfen, ob das Dateiverzeichnis beschreibbar ist.";
$lang['imglib_rotate_unsupported'] = "Das Drehen von Bildern wird nicht vom Server unterstützt.";
$lang['imglib_libpath_invalid'] = "Der Pfad zur Bildbibliothek ist falsch. Bitte den richtigen Pfad in den Einstellungen angeben.";
$lang['imglib_image_process_failed'] = "Bildverarbeitung fehlgeschlagen. Bitte prüfen, ob der Server das ausgewählte Protokoll unterstützt und der Pfad zur Bildbibliothek richtig ist.";
$lang['imglib_rotation_angle_required'] = "Ein Winkel muss zum Drehen des Bildes angegeben werden.";
$lang['imglib_writing_failed_gif'] = "GIF Bild.";
$lang['imglib_invalid_path'] = "Der Pfad zur Bilddatei ist nicht korrekt.";
$lang['imglib_copy_failed'] = "Das Kopieren des Bildes ist fehlgeschlagen.";
$lang['imglib_missing_font'] = "Es konnte keine nutzbare Schriftart gefunden werden.";
$lang['imglib_save_failed'] = "Bild konnte nicht gespeichert werden. Bitte überprüfen, ob das Bildverzeichnis beschreibbar ist.";


/* End of file imglib_lang.php */
/* Location: ./system/language/english/imglib_lang.php */
/* Translation by: Kovah - www.kovah.de */