<?php

$lang['migration_none_found']			= "Keine Migrationen gefunden";
$lang['migration_not_found']			= "Die Migration konnte nicht gefunden werden.";
$lang['migration_multiple_version']		= "Dies sind mehrere Migrationen mit der gleichen Versionsnummer: %d.";
$lang['migration_class_doesnt_exist']	= "Die Migrationsklasse \"%s\" konnte nicht gefunden werden.";
$lang['migration_missing_up_method']	= "Der Migrationsklasse \"%s\" fehlt eine 'up' Methode.";
$lang['migration_missing_down_method']	= "Der Migrationsklasse \"%s\" fehlt eine 'down' Methode.";
$lang['migration_invalid_filename']		= "Die Migration \"%s\" hat einen ungültigen Dateinamen..";


/* End of file migration_lang.php */
/* Location: ./system/language/english/migration_lang.php */
/* Translation by: Kovah - www.kovah.de */