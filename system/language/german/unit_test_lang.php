<?php

$lang['ut_test_name']		= 'Test Name';
$lang['ut_test_datatype']	= 'Test Datentyp';
$lang['ut_res_datatype']	= 'Erwarteter Datentype';
$lang['ut_result']			= 'Ergebnis';
$lang['ut_undefined']		= 'Undefinierter Test Name';
$lang['ut_file']			= 'Datei Name';
$lang['ut_line']			= 'Linienzahl';
$lang['ut_passed']			= 'Bestanden';
$lang['ut_failed']			= 'Fehlgeschlagen';
$lang['ut_boolean']			= 'Boolean';
$lang['ut_integer']			= 'Integer';
$lang['ut_float']			= 'Float';
$lang['ut_double']			= 'Float'; // can be the same as float
$lang['ut_string']			= 'String';
$lang['ut_array']			= 'Array';
$lang['ut_object']			= 'Object';
$lang['ut_resource']		= 'Resource';
$lang['ut_null']			= 'Null';
$lang['ut_notes']			= 'Notizen';


/* End of file unit_test_lang.php */
/* Location: ./system/language/english/unit_test_lang.php */
/* Translation by: Kovah - www.kovah.de */