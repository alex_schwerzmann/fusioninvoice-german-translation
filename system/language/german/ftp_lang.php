<?php

$lang['ftp_no_connection']			= "Es konnte keine gültige Verbindungs-ID gefunden werden. Bitte überprüfen, ob eine Verbindung besteht, bevor Datenroutinen ausgeführt werden.";
$lang['ftp_unable_to_connect']		= "Es konnte keine FTP Verbindung mit dem angegebenen Hostname aufgebaut werden.";
$lang['ftp_unable_to_login']		= "Es konnte keine FTP Verbindung aufgebaut werden. Bitte Benutzername und Kennwort prüfen.";
$lang['ftp_unable_to_makdir']		= "Das angegebene Verzeichnis konnte nicht erstellt werden.";
$lang['ftp_unable_to_changedir']	= "Das Verzeichnis konnte nicht gewechselt werden.";
$lang['ftp_unable_to_chmod']		= "Dateiberechtigungen konnten nicht gesetzt werden. Bitte Pfad prüfen. Hinweis: Diese Funktion ist nur mit PHP5 und höher verfügbar.";
$lang['ftp_unable_to_upload']		= "Datei konnte nicht hochgeladen werden. Bitte Pfad prüfen.";
$lang['ftp_unable_to_download']		= "Datei konnte nicht heruntergeladen werden. Bitte Pfad prüfen.";
$lang['ftp_no_source_file']			= "Quelldatei konnte nicht gefunden werden. Bitte Pfad prüfen.";
$lang['ftp_unable_to_rename']		= "Datei konnte nicht umbenannt werden.";
$lang['ftp_unable_to_delete']		= "Datei konnte nicht gelöscht werden.";
$lang['ftp_unable_to_move']			= "Datei konnte nicht verschoben werden. Bitte prüfen, ob das Zielverzeichnis existiert.";


/* End of file ftp_lang.php */
/* Location: ./system/language/english/ftp_lang.php */
/* Translation by: Kovah - www.kovah.de */