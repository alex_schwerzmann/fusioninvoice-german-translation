
Deutsche Übersetzung für FusionInvoice
http://www.fusioninvoice.com/

# Installation #
Unter dem Tab "Downloads" ist das Paket de_DE.zip verfügbar.
Lade dieses herunter, entpacke es und lade die enthaltenen Dateien in den Ordner
deiner FusionInvoice Installation.
In den Einstellungen musst du lediglich noch auf Deutsch bzw. German umstellen.

# Zusätzliche Informationen #
Das Paket de_DE+system.zip enthält zusätzlich zu den Übersetzungen für das User
Interface die Übersetzungen für das System selber. Für den normalen Gebrauch sind
diese Übersetzungen jedoch nicht notwendig.

Mitwirkende:
- Kovah (www.kovah.de)

############

German translation files for FusionInvoice
http://www.fusioninvoice.com/

Contributors:
- Kovah (www.kovah.de)

Great thanks to Jesse Terry for building such an awesome application!

